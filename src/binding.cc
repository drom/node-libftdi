#include <nan.h>
#include <libftdi1/ftdi.h>
#include "jshandle.h"
#include "macros.h"

class ftdiContext : public JSHandle<ftdiContext> {
public:
    static const char *jsClassName() { return "ftdiContext"; }
};

using namespace v8;

NAN_METHOD(ftdi_new) {
    ftdi_context *cxt = ftdi_new();
    Local<Object> jscxt = ftdiContext::New(cxt);
    info.GetReturnValue().Set(jscxt);
}

NAN_METHOD(ftdi_usb_open) {
    ASSERT_OBJECT(info[0], jsctx)
    ftdi_context *cxt = (struct ftdi_context *)ftdiContext::Resolve(jsctx);
    ASSERT_UINT(info[1], vendor)
    ASSERT_UINT(info[2], product)
    CALL_LIBFTDI_INT(ftdi_usb_open(cxt, vendor, product))
}

NAN_METHOD(ftdi_usb_close) {
    ASSERT_OBJECT(info[0], jsctx)
    ftdi_context *cxt = (struct ftdi_context *)ftdiContext::Resolve(jsctx);
    CALL_LIBFTDI_INT(ftdi_usb_close(cxt))
}

NAN_MODULE_INIT(Init) {
    EXPORT_FUNCTION(ftdi_new)
    EXPORT_FUNCTION(ftdi_usb_open)
    EXPORT_FUNCTION(ftdi_usb_close)
}

NODE_MODULE(libftdi, Init)

#undef CDATA
#undef LOCAL_STRING
#undef LOCAL_FUNCTION
#undef EXPORT_NUMBER
#undef EXPORT_FUNCTION
#undef CALL_LIBFTDI_BOOL
#undef CALL_LIBFTDI_INT
#undef CALL_LIBFTDI_UINT
#undef CALL_LIBFTDI_CONST_CHAR
#undef CALL_LIBFTDI_CONST_CHAR_OR_NULL
#undef ASSERT_UINT
#undef ASSERT_BUFFER
#undef ASSERT_OBJECT
#undef ASSERT_SCAN_CXT
#undef ASSERT_CXT
#undef ASSERT_CXT_INFO
#undef ASSERT_FUNCTION
