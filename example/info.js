#!/usr/bin/env node
'use strict';

const ftdi = require('bindings')('ftdi.node');

const cxt = ftdi.ftdi_new();

ftdi.ftdi_usb_open(cxt, 0x0403, 0x6010);

ftdi.ftdi_usb_close(cxt);
